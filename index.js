const express = require("express"); //import thu viện express
const path = require("path"); //import thư viện đường dẫn
// Khởi tạo express
const app = express();
//khởi tạo cổng
const port = 8000
// khai báo thư viện mongoose
const mongoose = require("mongoose");
const { ProductRouter } = require("./app/router/router");
// nơi khai báo các router
//khai báo tránh lỗi Access-Control-Allow-Origin do CORS Middleware validate liên quan đến vấn đề bảo mật 
app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});

//Sử dụng được bodyJson
app.use(express.json());
//Sử dụng được unicode
app.use(express.urlencoded({
    extended: true
}));
// mongoose set
mongoose.set("strictQuery", true);

// kết nối với mongodb
mongoose.connect(`mongodb://127.0.0.1:27017/shop24h`, function (error) {
    if (error) throw error;
    console.log("Kết nối thành công với mongoDB")
})
//Nơi sử dụng các router
app.use(ProductRouter);

app.listen(port, () => {
    console.log(`app đã chạy trên cổng ${port}`)
})