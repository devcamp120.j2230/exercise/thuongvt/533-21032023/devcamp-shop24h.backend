const express = require("express");
const ProductMiddleware = require ("../middleware/middleware");
const ProductController = require("../controller/controller");

const ProductRouter = express.Router()

ProductRouter.post("/products",ProductMiddleware.shop24hMiddleware,ProductController.createProduct);
ProductRouter.get("/products",ProductMiddleware.shop24hMiddleware,ProductController.getAllProduct)
ProductRouter.get("/products/:ProductId",ProductMiddleware.shop24hMiddleware,ProductController.getProductById);
ProductRouter.put("/products/:ProductId",ProductMiddleware.shop24hMiddleware,ProductController.updateProduct);
ProductRouter.delete("/products/:ProductId",ProductMiddleware.shop24hMiddleware,ProductController.deleteProduct);
module.exports = {ProductRouter}