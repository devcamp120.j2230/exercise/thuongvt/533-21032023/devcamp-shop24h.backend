//Khai báo thưu viện mongose
const mongoose = require("mongoose");
// Khia báo schema
const schema = mongoose.Schema;
//khai báo thuộc tính
const ProductSchema = new schema({
    _id:{
        type:mongoose.Types.ObjectId,
        require:true
    },
    Name:{
        type: String,
        require: true
    },
    Type:{
        type: String,
        require: true
    },
    mageUrl:{
        type: String,
        require: true
    },
    BuyPrice:{
        type: Number,
        require:true
    },
    PromotionPrice:{
        type: Number,
        require:true
    },
    Description:{
        type: String,
        require: true
    }
},{timestamps:true})

module.exports = mongoose.model("Product", ProductSchema);