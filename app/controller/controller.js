// khai báo thư viện mongosee
const mongoose = require("mongoose");
// Khia báo model
const ProductModel = require("../model/model");

const createProduct = (req, res) => {
    // chuẩn bị dữ liệu
    let body = req.body;
    // kiểm tra dữ liệu
    if (!body.Name) {
        return res.status(400).json({
            message: err.message
        })
    }
    if (!body.Type) {
        return res.status(400).json({
            message: err.message
        })
    }
    if (!body.mageUrl) {
        return res.status(400).json({
            message: err.message
        })
    }
    if (!body.BuyPrice) {
        return res.status(400).json({
            message: err.message
        })
    }
    if (!body.PromotionPrice) {
        return res.status(400).json({
            message: err.message
        })
    }
    if (!body.Description) {
        return res.status(400).json({
            message: err.message
        })
    }

    // gọi model thực hiện thao tác nhiệm vụ
    let newProductData = {
        _id: mongoose.Types.ObjectId(),
        Name: body.Name,
        Type: body.Type,
        mageUrl: body.mageUrl,
        BuyPrice: body.BuyPrice,
        PromotionPrice: body.PromotionPrice,
        Description: body.Description
    }

    ProductModel.create(newProductData, (err, data) => {
        if (err) {
            return res.status(500).json({
                message: err.message
            })
        }
        return res.status(201).json({
            message: "Tạo dữ liệu thành công",
            Data: data
        })
    })
}

// get all
const getAllProduct =(req,res)=>{
    //B1: thu thập dữ liệu từ req
    let productsType = req.query.Type
    let condition = {}
    //B2: validate dữ liệu
    if(productsType){
        condition.Type = productsType
    }
    ProductModel.find(condition).limit().exec((err,data)=>{
        if(err){
            return res.status(500).json({
                message: err.message
            })
        }
        return res.status(200).json({
            message:"tải toàn bộ dữ liệu thành công",
            Data: data
        })
    })
}
// getproduct By Id
const getProductById = (req,res)=>{
    let Id = req.params.ProductId;
    if(!mongoose.Types.ObjectId.isValid(Id)){
        return res.status(500).json({
            message: err.message
        })
    }
    ProductModel.findById(Id).exec((err,data)=>{
        if(err){
            return res.status(500).json({
                message: err.message
            })
        }
        return res.status(200).json({
            message:"tải dữ liệu thành công thông qua id",
            Data: data
        })
    })
}
// update Product
const updateProduct = (req,res)=>{
    let Id = req.params.ProductId;
    let body = req.body;

    if(!mongoose.Types.ObjectId.isValid(Id)){
        return res.status(500).json({
            message: err.message
        })
    }
    if (!body.Name) {
        return res.status(400).json({
            message: err.message
        })
    }
    if (!body.Type) {
        return res.status(400).json({
            message: err.message
        })
    }
    if (!body.mageUrl) {
        return res.status(400).json({
            message: err.message
        })
    }
    if (!body.BuyPrice) {
        return res.status(400).json({
            message: err.message
        })
    }
    if (!body.PromotionPrice) {
        return res.status(400).json({
            message: err.message
        })
    }
    if (!body.Description) {
        return res.status(400).json({
            message: err.message
        })
    }

    let productUpdate = {
        Name: body.Name,
        Type: body.Type,
        mageUrl: body.mageUrl,
        BuyPrice: body.BuyPrice,
        PromotionPrice: body.PromotionPrice,
        Description: body.Description
    }

    ProductModel.findByIdAndUpdate(Id,productUpdate,(err,data)=>{
        if(err){
            return res.status(500).json({
                message: err.message
            })
        }
        return res.status(200).json({
            message:"Update dữ liệu thành công thông qua id",
            Data: data
        })
    })
}

//delete
const deleteProduct = (req,res)=>{
    let Id = req.params.ProductId;

    ProductModel.findByIdAndDelete(Id).exec((err,data)=>{
        if(err){
            return res.status(500).json({
                message: err.message
            })
        }
        return res.status(200).json({
            message:"Delete dữ liệu thành công thông qua id",
            Data: data
        })
    })
}
module.exports = {
    createProduct,
    getAllProduct,
    getProductById,
    updateProduct,
    deleteProduct
}